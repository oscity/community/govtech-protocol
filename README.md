# GovTech Protocol

The GovTech Protocol is a W3C Verifiable Claims' compliant protocol proposed to serve as the common ground for public institutions to issue and manage citizens' official documents as digital verifiable credentials that anyone can easily verify. 

# **Standards Alignment**

OS City's GovTech Protocol is originated on [**Blockcerts**](https://www.blockcerts.org), which uses open standards for recipient-centric certificates, and [**OpenCerts**](https://docs.opencerts.io/docs/), which is part of Singapore’s Smart Nation initiative and enables issuance and validation of tamper-resistant certificates in a convenient and reliable manner. At the same time, [OpenCerts](https://docs.opencerts.io/docs/) is built atop of [**OpenAttestation**](https://www.openattestation.com/docs/getting-started), which is an open source framework for verifiable document and transferable records. All of these helped us think of **a reliable protocol to hold citizens' official documents as recipient-centric, tamper-resistant, verifiable documents and transferable records.**

Through [Blockcerts](https://www.blockcerts.org), [OpenCerts](https://docs.opencerts.io/docs/), and [OpenAttestation](https://www.openattestation.com/docs/getting-started) the proposed protocol conforms to the above standards, demonstrating how such standards can be used in a viable system, including:

- revocation and expiration of certificates,
- identity of participants in a decentralized, self-sovereign mode,
- dynamic rendering for a better user experience,
- selective disclosure for privacy protection, and
- other lifecycle concerns, including key management

The standard, as well as the design philosophy behind **[OS City Open Source Community repositories](https://gitlab.com/oscity/community)**,  is aligned with (and willing to contributing to) the following standards:

- [IMS Open Badges](https://www.imsglobal.org/sites/default/files/Badges/OBv2p0/index.html)
- [W3C Verifiable Claims](https://w3c.github.io/vc-data-model/)
- [W3C Linked Data Signatures](https://w3c-dvcg.github.io/ld-signatures/)
- [W3C / Rebooting Web of Trust Decentralized Identifiers](https://github.com/WebOfTrustInfo/rebooting-the-web-of-trust-fall2016/blob/master/draft-documents/DID-Spec-Implementers-Draft-01.pdf)

All of our repositories at **[OS City Open Source Community repositories](https://gitlab.com/oscity/community)** including this first draft have been designed under the **[Principles for Development](https://digitalprinciples.org/)** with particular focus on:

- [Design With the User](https://digitalprinciples.org/principle/design-with-the-user/)
- [Use Open Standards, Open Data, Open Source, and Open Innovation](https://digitalprinciples.org/principle/use-open-standards-open-data-open-source-and-open-innovation/)
- [Design for Scale](https://digitalprinciples.org/principle/design-for-scale/)
- [Address Privacy & Security](https://digitalprinciples.org/principle/address-privacy-security/)
- [Reuse and Improve](https://digitalprinciples.org/principle/reuse-and-improve/)
- [Be Collaborative](https://digitalprinciples.org/principle/be-collaborative/)

# Proposed Schema

Blockchain Certificate schemas implement those of [Verifiable Credentials](https://www.w3.org/TR/vc-data-model/). As with Verifiable Credentials, we've provided both a JSON-LD context and JSON schema. The purpose of the JSON-LD context is to map types to Internationalized Resource Identifiers (IRIs), providing semantic context for data. The JSON Schema is used for syntactic validation.

**Example**

```json
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://www.blockcerts.org/schema/3.0-alpha/context.json",
    "https://www.w3.org/2018/credentials/examples/v1"
  ],
  "id": "urn:uuid:bbba8553-8ec1-445f-82c9-a57251dd731c",
  "type": [
    "VerifiableCredential", "OfficialDocumentCredential"
  ],  
	"issuer": {
		"id": "did:example:23adb1f712ebc6f1c276eba4dfa",
		"email": "info@issuer.com",	
		"name": "My Issuer",
		"revocationList": "https://issuer.com/revocation.json",
		"type": "Profile",
		"url": "https://issuer.com"
	},
  "issuanceDate": "2010-01-01T19:73:24Z",
	"expirationDate": "2020-01-01T19:23:24Z",
  "credentialSubject": {
    "id": "did:example:ebfeb1f712ebc6f1c276e12ec21",
    "givenName": "Jayden",
    "familyName": "Doe",
    "email": "jay@den.com",
    "phone": "+5218115778648",
    "spouse": "did:example:c276e12ec21ebfeb1f712ebc6f1"
		"credentialData": {
	      "type": "Permit",
	      "name": "Construction Permit",
				"status": "Autorized",
				"description": "Lorem ipsum",
				"address": "Lorem ipsum"
    }
  },
  "privacy": {
			"obfuscatedData": [
	      "cdcc4c34f57718d0b1126dc79f874c8be328034c18038f96d4a26d3eafd24792",
	      "101285d7c89746ab6fa2062c1986bb88dbd321db3b6634fd203a569f741eaf2c"
	    ]
	},
	"template": {
      "name": "GOVTECH_DEMO",
      "type": "EMBEDDED_RENDERER",
      "url": "https://demo-renderer.opencerts.io"
  },
  "proof": {
    "type": "MerkleProof2019",
    "created": "2020-01-21T12:32:11.693759",
    "proofValue": "z2LuLBVSfnVzaQtvzuA7EaPQsGEgYWeaMTH1p3uqAG3ESx9HYyFzFFrYsyPkZSbn1Ji5LN76jw6HBr3oiaa8KsQenCPqKk7dJvxEXsDnYvhuDHtsrSRbzHdJKd66jAowkzPxPFi3ivyAv7WRK1WV2VhegYVQEnCBTrGJWFUMFFXunTcus7ZyedQvS4sr61X2y8QuJ57ycB5JMEHvUgAVq3qh2g3ucehg2ERKLo98jmqTcsh9HThkECG3BTNYRD3QL7AHWPjxRbQNSA83QNYXcCNA7NaZnCWyjC17ZBj3xszp76XvqFRrLjQbRSbzjVTPtBSV8QjhxThT3KTfgwjRn5JeeXhYvebsTT9YGL3W4ufzFRDpH79n5KPiaj1BPbEUfUq7vf2dg26QWeZBi7ME56",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "ecdsa-koblitz-pubkey:0x7e30a37763e6Ba1fFeDE1750bBeFB4c60b17a1B3"
  }
}
```

Implements Verifiable Credential v1.0 schema: [https://www.w3.org/TR/vc-data-model/](https://www.w3.org/TR/vc-data-model/)

The schema defines the following properties:

## **`id` (string, required)**

Defined by `id` property in [https://www.w3.org/TR/vc-data-model/#identifiers](https://www.w3.org/TR/vc-data-model/#identifiers).

## **`type` (JsonLdType, required)**

Defined by `type` property of [https://www.w3.org/TR/vc-data-model/#types](https://www.w3.org/TR/vc-data-model/#types)

## **`issuer` (Issuer, required)**

Defined by `issuer` property of [https://www.w3.org/TR/vc-data-model/#issuer](https://www.w3.org/TR/vc-data-model/#issuer).

## **`issuanceDate` (DateTime, required)**

Defined by `issuanceDate` property of [https://www.w3.org/TR/vc-data-model/#issuance-date](https://www.w3.org/TR/vc-data-model/#issuance-date).

## **`expirationDate` (DateTime, required)**

Defined by `expirationDate` property of [https://www.w3.org/TR/vc-data-model/#expiration-date](https://www.w3.org/TR/vc-data-model/#expiration).

## **`credentialSubject` (object, required)**

Defined by `credentialSubject` property of [https://www.w3.org/TR/vc-data-model/#credential-subject](https://www.w3.org/TR/vc-data-model/#credential-subject).

## **`privacy` (object, required)**

Defined by `privacy` property of [https://www.openattestation.com/](https://www.openattestation.com/) for Selective Disclosure.

## **`template` (object, required)**

Defined by `template` property of [https://www.openattestation.com/](https://www.openattestation.com/) for Decentralized Rendering.

## **`proof` (MerkleProof2019, required)**

Defined by `proof` property of [https://www.w3.org/TR/vc-data-model/#proofs-signatures](https://www.w3.org/TR/vc-data-model/#proofs-signatures).

---

Sources and references.

[Verifiable Credentials Data Model v1.1](https://www.w3.org/TR/vc-data-model/)

[Blockchain Credentials](https://www.blockcerts.org/)

[cert-schema/schema-3.0-alpha.md at master · blockchain-certificates/cert-schema](https://github.com/blockchain-certificates/cert-schema/blob/master/docs/schema-3.0-alpha.md)

[OpenCerts - An easy way to check and verify your OpenCerts certificates](https://www.opencerts.io/)

[Getting started · OpenCerts](https://docs.opencerts.io/docs/)

[OpenCerts Transcripts · OpenCerts](https://docs.opencerts.io/docs/transcripts)

[OpenAttestation | OpenAttestation](https://www.openattestation.com/)

[Overview | OpenAttestation](https://www.openattestation.com/docs/verifiable-document/overview-document-store/)


## Contributing
We are open to contributions under the described standards alignment.

## License
MIT License

## Project status
Under development, expecting first use case and lessons for improvement.
